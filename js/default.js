(function ($) {
  Drupal.behaviors.videoSpinner = {
    attach: function (context, settings) {
	  if (context !== document) {
        return;
      }

      var di = 0;
	  var ti = 0;
	  $('.media-video-spinner').bind('touchstart mousedown', function(event) {
	    var overlay = $(this).parent().find('.media-video-spinner-overlay');
		if ($(overlay).css('display') != 'none')
		  $(this).parent().find('.media-video-spinner-overlay').css('display', 'none');
		  
		$(this).attr('data-spin', true);
		
		ti = $(this)[0].currentTime;

		var di = 0;
		if (event.type == 'touchstart') {
		  di = event.changedTouches[0].pageX;
		} else {
		  di = event.pageX;
		}
		
		event.preventDefault();
	  });
	  
	  $(window).bind('touchmove mousemove', function(event) {
		var spinner = $('.media-video-spinner[data-spin="true"]');
		if ($(spinner).length) {
			
		  var pageX = 0;
		  if (event.type == 'touchmove') {
		    pageX = event.changedTouches[0].pageX;
		  } else {
		    pageX = event.pageX;
		  }
		
		  duration = $(spinner)[0].duration;
		  currentTime = $(spinner)[0].currentTime;
		  dx = (di - pageX) / ($(window).width() / 2)
		  offset = -1 * dx * duration;

		  currentTime = (duration + (offset + ti)) % duration;

		  $(spinner)[0].currentTime = currentTime;		
		}
	  });
	  
	  $(window).bind('touchend touchcancel mouseup', function(event) {
		$('.media-video-spinner[data-spin="true"]').removeAttr('data-spin');
	  });
    }
  };
}(jQuery));