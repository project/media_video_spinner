<?php

namespace Drupal\media_video_spinner\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\media\Entity\MediaType;
use Drupal\media\Plugin\media\Source\VideoFile;

/**
 * Plugin implementation of the 'Video Spinner' formatter.
 *
 * @FieldFormatter(
 *   id = "video_spinner_formatter",
 *   label = @Translation("Video Spinner"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class VideoSpinnerFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Summary');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
	  $fid = $item->getValue()['target_id'];
	  $file = \Drupal\file\Entity\File::load($fid);
	  $url = file_url_transform_relative(file_create_url($file->getFileUri()));
	  $element[$delta] = [
	    '#theme' => 'video_spinner',
		'#url' => $url
	  ];
    }
	
	$element['#attached']['library'][] = 'media_video_spinner/default';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
	if ($field_definition->getTargetEntityTypeId() !== 'media') {
      return FALSE;
    }

    if (parent::isApplicable($field_definition)) {
      $media_type = $field_definition->getTargetBundle();
      if ($media_type) {
        $media_type = MediaType::load($media_type);
        return $media_type && $media_type->getSource() instanceof VideoFile;
      }
    }
    return FALSE;
  }
}